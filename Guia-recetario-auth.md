# Recetario Angular Auth

Temas

- Autenticación en Angular con Firebase auth
- Almacenamiento de datos en Angular con Firebase firestore
- Rutas protegidas (protected routes)
- Formularios dinámicos (responsive forms)

## Pasos iniciales

En caso de no tener el repositorio en máquina local:

- Crear directorio del proyecto y descargar código ejecutando: $ git clone https://gitlab.com/j_sauceda/recetario-angular-simple.git
- $ cd recetario-angular-simple
- $ npm install
- Comprobar que el proyecto se ejecuta: $ ng serve

## Firebase

- Los pasos a seguir están inspirados en la desactualizada guía [Angular Fire Quickstart](https://github.com/angular/angularfire/blob/master/docs/install-and-setup.md)
- Crear cuenta [en el sitio oficial de Firebase](https://firebase.google.com/)
- (Opcional) leer [la documentación oficial de Firebase](https://firebase.google.com/docs)
- (Opcional) ver [video de autenticación con Firebase](https://www.youtube.com/watch?v=8VTxuIvMTlc)
- Crear un proyecto nuevo en la [consola de Firebase](https://console.firebase.google.com) // en este proyecto no usaremos Google Analytics


### Configurar el acceso a la aplicación:
- Dar click en [agregar Firebase a una aplicación Web](https://console.firebase.google.com/project/<nombre_proyecto>/overview):
- Register app: ingresar nombre del proyecto angular. En este proyecto no usaremos Firebase Hosting
- Instalar Firebase SDK: $ npm install firebase
- NOTA: No es necesario copiar el script de inicialización de Firebase, este se importará luego de modo automático con @angular/fire:

### Configurar la autenticación:
- Dar click en `Authentication` en la página de nuestro proyecto en la consola de Firebase:
- Seleccionar `Get started`
- Seleccionar `Email/Password`, sin `passwordless sign-in`
- Seleccionar `Save`
- Seleccionar `Add new provider`
- Seleccionar `Google` > `Enable`
- Editar `Project public-facing name: Recetario Angular-auth` y `Project support email: <correo-personal>@gmail.com`
- Seleccionar `Save`
- NOTA: no se usarán otros proveedores en este proyecto.

### Configurar la Base de Datos:

- En el panel izquierdo, dar click en `Build` > `Firestore Database`
- Seleccionar `Create database` > `Test mode`
- Seleccionar Cloud Firestore location acorde a nuestra ubicación geográfica
- Iniciar una colección:
  - Parent path `/`
  - Collection ID: `recetas`
  - Add its first document (ejemplo corto):
    - Document ID: `Auto-ID` o `Tacos`
    - Field `autor`, Type `string`, Value `admin`
    - Field `nombre`, Type `string`, Value `Tacos mexicanos de carne`
    - Field `imagen`, Type `string`, Value `https://imag.bonviveur.com/presentacion-principal-de-los-tacos-mexicanos-de-carne_1000.webp`
    - Field `introduccion`, Type `string`, Value `¿A quién no le gustan los tacos mexicanos de carne? Este es uno de los platillos más populares de la gastronomía mexicana y apetecen a casi cualquier hora. En casa, se preparan fácilmente y es una receta ideal para compartir con familia o amigos.`
    - Field `ingredientes`, Type `array`, Values `Tacos`
    - Field `preparacion`, Type `array`, Value `Comprar y comer tacos`
    - Field `consejos`, Type `array`, Value `Comprar tacos apetitosos`
    - Field `fuente`, Type `string`, Value `https://www.bonviveur.es/recetas/tacos-mexicanos-de-carne`
  - Click en `Save` > El documento debe aparecer en pantalla

### Instalar y configurar Firebase CLI:

- $ npm install -g firebase-tools
- $ firebase login
- Establecer variables de entorno. [Mayor información](https://angular.io/guide/build)
- crear directorio /src/environments: $ mkdir environments
- $ cd environments
- Crear archivos environment.ts: $ touch environment.ts
- Editar environment.ts:

```javascript
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
```

- Crear archivo environment.prod.ts: $ touch environment.prod.ts
- Editar environment.prod.ts:

```javascript
export const environment = {
  production: true,
};
```

### Instalar y configurar AngularFire:
- $ ng add @angular/fire:
  - Would you like to proceed? Yes
  - What features would you like to setup? Authentication + Firestore
  - Allow Firebase to collect CLI and Emulation suite usage and error reporting information? yes/no
  - Which Firebase account would you like to use? 'correo-personal'@gmail.com
  - Please select a project: recetario-angular-auth
  - Please select an app: recetario
  - NOTA: el comando anterior instala paquetes y actualiza los ficheros .gitignore, /src/app/app.module.ts y /src/environment.ts
- Revisar app.module.ts: debe incluir tres import statements desde @angular/fire

### Habilitar Formularios reactivos

- Editar app.module.ts:

```javascript
...
import { ReactiveFormsModule } from '@angular/forms';
...
imports: [
    ...
    ReactiveFormsModule,
  ],
```

### Configurar Firebase, crear formularios reactivos y definir rutas

- Los siguientes pasos están inspirados en [la documentación oficial](https://firebase.google.com/docs/auth/web/start)
- Crear servicio user: $ ng g service services/user
- Editar user.service.ts:

```javascript
import { Injectable } from '@angular/core';
import {
  Auth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
  signInWithPopup,
  GoogleAuthProvider,
} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private auth: Auth) {}

  register({ email, password }: any) {
    return createUserWithEmailAndPassword(this.auth, email, password)
      .then((userCredentials) => {
        // console.log( `Successful registration with credentials: ${JSON.stringify(userCredentials)}` );
      })
      .catch((error) => console.error(error));
  }

  login({ email, password }: any) {
    return signInWithEmailAndPassword(this.auth, email, password)
      .then((userCredentials) => {
        // console.log( `Successful login with credentials: ${JSON.stringify(userCredentials)}` );
      })
      .catch((error) => {
        console.error(error);
      });
  }

  google_login() {
    return signInWithPopup(this.auth, new GoogleAuthProvider());
  }

  is_user_logged_in() {
    const user = this.auth.currentUser;
    if (user) {
      return true;
    }
    return false;
  }

  get_user() {
    const user = this.auth.currentUser;
    if (user) {
      return {
        displayName: user.displayName,
        email: user.email,
        photoURL: user.photoURL,
        emailVerified: user.emailVerified,
        uid: user.uid,
      };
    } else {
      return null;
    }
  }

  logout() {
    console.log('User logged out');
    return signOut(this.auth);
  }
}
```

- Crear componente registro: $ ng g component components/registro
- Editar registro.component.ts:

```javascript
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
})
export class RegistroComponent implements OnInit {
  formReg!: FormGroup;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.formReg = new FormGroup({
      email: new FormControl(),
      password: new FormControl(),
    });
  }

  onSubmit() {
    this.userService
      .register(this.formReg.value)
      .then((response) => {
        // console.log(response);
        this.router.navigate(['/entrada']);
      })
      .catch((error) => console.error(error));
  }

  onGoogleLogin() {
    this.userService
      .google_login()
      .then((response) => {
        // console.log(response);
        this.router.navigate(['/perfil']);
      })
      .catch((error) => console.error(error));
  }
}
```

- Editar registro.component.html:

```xml
<div
  class="ui middle aligned center aligned grid"
  [ngStyle]="{ 'margin-top': '10%', 'margin-bottom': 'justify' }"
>
  <div [ngStyle]="{ 'max-width': '450px' }">
    <h2 class="ui teal header">Crea una cuenta de usuario</h2>
    <form [formGroup]="formReg" (ngSubmit)="onSubmit()" class="ui middle form">
      <div class="ui stacked segment">
        <div class="field">
          <label class="form-label">Correo electrónico</label>
          <input type="text" formControlName="email" />
        </div>
        <div class="field">
          <label class="form-label">Contraseña</label>
          <input type="password" formControlName="password" />
        </div>
        <input
          class="ui fluid large teal submit button"
          type="submit"
          value="Crear"
        />
      </div>
    </form>

    <div class="ui message">
      <button
        class="ui fluid large submit blue button"
        (click)="onGoogleLogin()"
      >
        Crear cuenta con Google
      </button>
    </div>
    <div class="ui message">
      ¿Ya tienes una cuenta?
      <a class="ui fluid large submit blue button" routerLink="/entrada">
        Entra a tu cuenta
      </a>
    </div>
  </div>
</div>
```

- Crear componente entrada: $ ng g component components/entrada
- Editar entrada.component.ts:

```javascript
import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-entrada',
  templateUrl: './entrada.component.html',
  styleUrls: ['./entrada.component.css'],
})
export class EntradaComponent {
  formLogin: FormGroup;

  constructor(private userService: UserService, private router: Router) {
    this.formLogin = new FormGroup({
      email: new FormControl(),
      password: new FormControl(),
    });
  }

  onSubmit() {
    this.userService
      .login(this.formLogin.value)
      .then((response) => {
        // console.log(response);
        this.router.navigate(['/perfil']);
      })
      .catch((error) => console.error(error));
  }

  onGoogleLogin() {
    this.userService
      .google_login()
      .then((response) => {
        // console.log(response);
        this.router.navigate(['/perfil']);
      })
      .catch((error) => console.error(error));
  }
}
```

- Editar entrada.component.html:

```xml
<div
  class="ui middle aligned center aligned grid"
  [ngStyle]="{ 'margin-top': '10%', 'margin-bottom': 'justify' }"
>
  <div [ngStyle]="{ 'max-width': '450px' }">
    <h2 class="ui teal header">Entra a tu cuenta</h2>
    <form
      [formGroup]="formLogin"
      (ngSubmit)="onSubmit()"
      class="ui middle form"
    >
      <div class="ui stacked segment">
        <div class="field">
          <label class="form-label">Correo electrónico</label>
          <input type="text" formControlName="email" />
        </div>
        <div class="field">
          <label class="form-label">Contraseña</label>
          <input type="password" formControlName="password" />
        </div>
        <input
          class="ui fluid large teal submit button"
          type="submit"
          value="Entrar"
        />
      </div>
    </form>

    <div class="ui message">
      <button
        class="ui fluid large submit blue button"
        (click)="onGoogleLogin()"
      >
        Entra con tu cuenta de Google
      </button>
    </div>
    <div class="ui message">
      ¿Todavía no tienes una cuenta?
      <a class="ui fluid large submit blue button" routerLink="/registro">
        Crea tu cuenta
      </a>
    </div>
  </div>
</div>
```

- Crear componente salida: $ ng g component components/salida
- Editar salida.component.ts:

```javascript
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-salida',
  templateUrl: './salida.component.html',
  styleUrls: ['./salida.component.css'],
})
export class SalidaComponent {
  constructor(private userService: UserService, private router: Router) {}

  onLogout() {
    this.userService
      .logout()
      .then(() => this.router.navigate(['/']))
      .catch((error) => console.log(error));
  }

  is_user_logged_in() {
    return this.userService.is_user_logged_in();
  }
}
```

- Editar salida.component.html:

```xml
<a class="item" *ngIf="is_user_logged_in()" (click)="onLogout()">Salida</a>
```

- Crear servicio perfil: $ ng g service services/perfil
- Editar perfil.service.ts:

```javascript
import { Injectable } from '@angular/core';
import {
  Firestore,
  addDoc,
  collection,
  getDocs,
  query,
  where,
} from '@angular/fire/firestore';

interface Perfil {
  user?: string;
  nombre: string;
  imagen: string;
  acerca: string;
}

@Injectable({
  providedIn: 'root',
})
export class PerfilService {
  constructor(private firestore: Firestore) {}

  get_perfil(id_perfil: string) {
    const perfilesRef = collection(this.firestore, 'perfiles');
    const perfilQuery = where('user', '==', id_perfil);
    const q = query(perfilesRef, perfilQuery);
    return getDocs(q);
  }

  add_perfil(nuevo_perfil: Perfil) {
    const perfilesRef = collection(this.firestore, 'perfiles');
    return addDoc(perfilesRef, nuevo_perfil);
  }
}
```

- Crear componente perfil: $ ng g component components/perfil
- Editar perfil.component.ts:

```javascript
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Receta } from 'src/app/Receta';
import { PerfilService } from 'src/app/services/perfil.service';
import { RecetasService } from 'src/app/services/recetas.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css'],
})
export class PerfilComponent implements OnInit {
  formulario!: FormGroup;
  perfil!: any;
  recetas!: any;
  usuario!: any;

  constructor(
    private fb: FormBuilder,
    private perfilService: PerfilService,
    private recetasService: RecetasService,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.usuario = this.userService.get_user();
    this.get_perfil();
    this.get_recetas_perfil();
    this.formulario = this.fb.group({
      nombre: ['', Validators.required],
      imagen: ['', Validators.required],
      acerca: [''],
    });
  }

  get_perfil() {
    this.perfilService.get_perfil(this.usuario.uid).then((docs) => {
      docs.forEach((docSnap) => {
        this.perfil = docSnap.data();
      });
    });
  }

  get_recetas_perfil() {
    this.recetas = this.recetasService.get_recetas_autor(this.usuario.uid);
  }

  onSubmit() {
    this.perfilService
      .add_perfil({
        user: this.usuario.uid,
        ...this.formulario.value,
      })
      .then((new_perfil) => {
        console.log(`Submitted profile: ${JSON.stringify(new_perfil)}`);
        this.get_perfil();
      })
      .catch((error) => console.error(error));
  }

  onUpdate(receta: Receta) {
    this.router.navigate([`/editar-receta/${receta.id}`]);
  }

  onDelete(receta: Receta) {
    this.recetasService.delete_receta(receta).then(() => {
      this.get_recetas_perfil();
      console.log('Receta deleted successfully');
    });
  }
}
```

- Editar perfil.component.html:

```xml
<div
  *ngIf="perfil"
  class="ui middle aligned stackable grid container"
  [ngStyle]="{ 'margin-top': '60px', 'margin-bottom': '60px' }"
>
  <!-- Información del autor -->
  <div class="row">
    <div class="six wide column">
      <div class="ui centered small image">
        <img *ngIf="perfil.imagen !== ''" src="{{ perfil.imagen }}" />
        <img
          *ngIf="perfil.imagen === ''"
          src="https://www.pinclipart.com/picdir/middle/8-82428_profile-clipart-generic-user-gender-neutral-head-icon.png"
        />
      </div>
    </div>
    <div class="ten wide column">
      <h3 class="ui header">
        <i [ngStyle]="{ color: 'gray' }" class="newspaper outline icon"></i>
        Acerca de @{{ perfil.nombre | lowercase }}
      </h3>
      <div class="ui segment">
        {{ perfil.acerca }}
      </div>
    </div>
  </div>

  <div class="ui divider"></div>

  <!-- Acciones de recetas -->
  <div class="row">
    <div class="eight wide column">
      <h3 class="ui header">Administrar recetas</h3>
    </div>
    <div class="eight wide column">
      <a class="ui fluid large blue button" routerLink="/nueva-receta">
        Publicar una nueva receta
      </a>
    </div>
  </div>

  <div class="ui divider"></div>

  <!-- Panel de recetas -->
  <table class="ui celled table" *ngIf="recetas && recetas.length > 0">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Introducción</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      <tr *ngFor="let receta of recetas">
        <td data-label="Nombre">{{ receta.nombre }}</td>
        <td data-label="Introducción">{{ receta.introduccion }}</td>
        <td data-label="Acciones">
          <div class="ui buttons">
            <div class="eight wide">
              <button class="ui fluid green button" (click)="onUpdate(receta)">
                Editar
              </button>
            </div>
            <div class="or"></div>
            <div class="eight wide">
              <button class="ui fluid red button" (click)="onDelete(receta)">
                Borrar
              </button>
            </div>
          </div>
        </td>
      </tr>
    </tbody>
  </table>

  <!-- Aviso de cero recetas -->
  <div class="ui segment content" *ngIf="!recetas">
    <p>Todavía no has publicado recetas</p>
  </div>
</div>

<!-- Forma de creación de perfil -->
<div
  *ngIf="!perfil"
  class="ui main text container"
  [ngStyle]="{
    'margin-top': '60px',
    'margin-bottom': '60px',
    'max-width': '450px'
  }"
>
  <h2 class="ui teal header">Crea tu perfil de usuario</h2>
  <form [formGroup]="formulario" (ngSubmit)="onSubmit()" class="ui middle form">
    <div class="ui stacked segment">
      <div class="field">
        <label class="form-label">Nombre de usuario</label>
        <input type="text" formControlName="nombre" />
      </div>
      <div class="field">
        <label class="form-label">Avatar</label>
        <input
          type="text"
          formControlName="imagen"
          placeholder="URL de una imagen"
        />
      </div>
      <div class="field">
        <label class="form-label">Sobre ti</label>
        <textarea
          type="text"
          formControlName="acerca"
          placeholder="Dinos un poco sobre tu persona"
        ></textarea>
      </div>
      <input
        class="ui fluid large teal submit button"
        type="submit"
        value="Crear"
      />
    </div>
  </form>
</div>
```

### Asegurar rutas con auth-guard

- Definir nuevas rutas en app.module.ts:

```javascript
...
import { canActivate, redirectUnauthorizedTo } from '@angular/fire/auth-guard'
...
const appRoutes: Routes = [
  { path: '', component: RecetasComponent },
  { path: 'acerca', component: AcercaComponent },
  { path: 'articulo/:id', component: ArticuloComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'entrada', component: EntradaComponent },
  {
    path: 'perfil',
    component: PerfilComponent,
    ...canActivate(() => redirectUnauthorizedTo(['entrada'])),
  },
];
...
```

- Crear componente navbar: $ ng g component components/navbar
- Editar navbar.component.ts:

```javascript
import { Component, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  @Input() title!: string;

  constructor(private userService: UserService) {}

  is_user_logged_in() {
    return this.userService.is_user_logged_in();
  }
}
```

- Editar navbar.component.html:

```xml
<div class="ui teal fixed inverted menu">
  <div class="ui container">
    <a routerLink="/" class="header teal item">
      {{ title }}
    </a>
    <a *ngIf="is_user_logged_in()" routerLink="/perfil" class="item">Perfil</a>
    <a *ngIf="!is_user_logged_in()" routerLink="/entrada" class="item"
      >Entrada</a
    >
    <a *ngIf="!is_user_logged_in()" routerLink="/registro" class="item"
      >Registro</a
    >
    <app-salida></app-salida>
  </div>
</div>
```

- Crear componente pie: $ ng g component components/pie
- Editar pie.component.html:

```xml
<div
  class="ui teal inverted vertical footer segment"
  [ngStyle]="{ position: 'fixed', bottom: '0px', width: '100%' }"
>
  <div class="ui center aligned container">
    <a routerLink="/acerca" class="ui link" [ngStyle]="{ color: 'white' }"
      >Acerca de nosotros</a
    >
  </div>
</div>
```

- Editar acerca.component.html:

```xml
<div
  class="ui middle aligned center aligned grid"
  [ngStyle]="{ 'margin-top': '10%', 'margin-bottom': 'justify' }"
>
  <div class="message">
    <div class="ui message">
      <p>Recetario Angular se encuentra en la versión 1.0.0 &copy; 2022</p>
      <a routerLink="/">Página Principal</a>
    </div>
  </div>
</div>
```

- Editar app.component.html:

```xml
<div class="ui middle aligned center aligned grid">
  <div class="ui column">
    <!-- Encabezado -->
    <app-navbar [title]="title"></app-navbar>
    <!-- Aquí va el blog de recetas -->
    <router-outlet></router-outlet>
    <!-- Pie de página -->
    <app-pie></app-pie>
  </div>
</div>
```

- Probar registro y login con email/password y con GoogleAccount:
  - Probar registro, entrada, salida, rutas seguras
  - Revisar creación de usuarios en Firebase console > Authentication
  - Revisar console log del navegador
- En caso de pruebas exitosas, comentar o borrar console.log() de los métodos onSubmit() y onGoogleSubmit() de registro.component.ts y entrada.component.ts

### Operaciones CRUD (creación, lectura, actualización y borrado) en Firestore DB

- La documentación oficial de Firebase provee mayor información sobre [la lectura de datos via DB queries](https://firebase.google.com/docs/firestore/query-data/get-data#web-version-9), [borrado](https://firebase.google.com/docs/firestore/manage-data/delete-data), [creación y edición](https://firebase.google.com/docs/firestore/manage-data/add-data) de datos en Firestore.
- Editar /src/app/Receta.ts:

```javascript
export interface Receta {
  id?: string;
  autor: string;
  nombre: string;
  imagen: string;
  editada?: Date;
  publicada?: Date;
  introduccion: string;
  ingredientes: string[];
  preparacion: string[];
  consejos: string[];
  fuente: string;
}
```

- Editar recetas.service.ts:

```javascript
import { Injectable } from '@angular/core';
import {
  Firestore,
  addDoc,
  collection,
  collectionData,
  deleteDoc,
  doc,
  getDoc,
  getDocs,
  query,
  setDoc,
  where,
} from '@angular/fire/firestore';
import { Receta } from '../Receta';

@Injectable({
  providedIn: 'root',
})
export class RecetasService {
  constructor(private firestore: Firestore) {}

  get_recetas() {
    const docRef = collection(this.firestore, 'recetas');
    return collectionData(docRef, { idField: 'id' });
  }

  get_receta(id_articulo: string) {
    const docRef = doc(this.firestore, 'recetas', id_articulo);
    return getDoc(docRef);
  }

  get_recetas_autor(id_autor: string) {
    let recetas: any[] = [];
    const q = query(
      collection(this.firestore, 'recetas'),
      where('autor', '==', id_autor)
    );
    const querySnapshot = getDocs(q);
    querySnapshot.then((documents) => {
      documents.forEach((doc) => {
        recetas.push({ id: doc.id, ...doc.data() });
      });
    });
    return recetas;
  }

  add_receta(nueva_receta: Receta) {
    const recetaRef = collection(this.firestore, 'recetas');
    return addDoc(recetaRef, nueva_receta);
  }

  update_receta(receta: Receta) {
    console.log(`Update receta.id: ${receta.id}`);
    const recetaRef = doc(this.firestore, `recetas/${receta.id}`);
    return setDoc(recetaRef, { editada: new Date(), ...receta });
  }

  delete_receta(receta: Receta) {
    const recetaRef = doc(this.firestore, `recetas/${receta.id}`);
    return deleteDoc(recetaRef);
  }
}
```

- Editar recetas.component.ts:

```javascript
import { Component, OnInit } from '@angular/core';
import { RecetasService } from 'src/app/services/recetas.service';

@Component({
  selector: 'app-recetas',
  templateUrl: './recetas.component.html',
  styleUrls: ['./recetas.component.css'],
})
export class RecetasComponent implements OnInit {
  recetas!: any[];

  constructor(private recetasService: RecetasService) {}

  ngOnInit(): void {
    this.recetasService.get_recetas().subscribe((all_recetas) => {
      this.recetas = all_recetas;
    });
    // console.log('recetas contains : ', this.recetas);
  }
}
```

- Editar recetas.component.html:

```xml
<div
  class="ui centered link cards"
  *ngIf="recetas.length > 0"
  [ngStyle]="{ 'margin-top': '60px', 'margin-bottom': '60px' }"
>
  <div *ngFor="let i of recetas" class="card">
    <a routerLink="/articulo/{{ i.id }}">
      <div class="ui image">
        <img src="{{ i.imagen }}" />
      </div>
      <div class="content">
        <h2 class="ui header">{{ i.nombre | titlecase }}</h2>
        <div class="description" *ngIf="i.introduccion.length > 400">
          {{ i.introduccion | slice : 0 : 400 }}...
        </div>
        <div class="description" *ngIf="i.introduccion.length <= 400">
          {{ i.introduccion }}
        </div>
      </div>
    </a>
  </div>
</div>

<div class="ui main text container" *ngIf="recetas.length === 0">
  <div class="ui active inverted dimmer">
    <div
      class="ui massive text loader"
      [ngStyle]="{ 'margin-top': '200px', 'margin-bottom': '60px' }"
    >
      Loading recetas... please be patient
    </div>
  </div>
</div>
```

- Editar articulo.component.ts:

```javascript
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { RecetasService } from 'src/app/services/recetas.service';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.css'],
})
export class ArticuloComponent implements OnInit {
  articulo: any = undefined;
  id_articulo!: string;

  constructor(
    private recetasService: RecetasService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.id_articulo = this.activatedRoute.snapshot.paramMap.get('id') || '';
    this.recetasService.get_receta(this.id_articulo).then((docSnap) => {
      this.articulo = { id: this.id_articulo, ...docSnap.data() };
      // console.log('articulo is: ', this.articulo);
    });
  }
}
```

- Editar articulo.component.html:

```xml
<div
  *ngIf="articulo"
  class="ui main text container"
  [ngStyle]="{ 'margin-top': '60px', 'margin-bottom': '60px' }"
>
  <h2 class="ui header">{{ articulo.nombre | titlecase }}</h2>
  <div class="ui centered medium image">
    <img src="{{ articulo.imagen }}" />
  </div>

  <div class="ui divider"></div>

  <h3 class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="newspaper outline icon"></i>
    Introducción
  </h3>
  <div class="ui segment content">
    {{ articulo.introduccion }}
  </div>

  <h3 class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="edit outline icon"></i>
    Ingredientes
  </h3>
  <div class="ui segment bulleted list">
    <div *ngFor="let i of articulo.ingredientes" class="item">
      {{ i }}
    </div>
  </div>

  <h3 class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="utensils icon"></i>
    Preparación
  </h3>
  <div class="ui segment bulleted list">
    <div *ngFor="let i of articulo.preparacion" class="item">
      {{ i }}
    </div>
  </div>

  <h3 *ngIf="articulo.consejos" class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="utensil spoon icon"></i>
    Consejos
  </h3>
  <div *ngIf="articulo.consejos" class="ui segment bulleted list">
    <div *ngFor="let i of articulo.consejos" class="item">
      {{ i }}
    </div>
  </div>

  <h3 class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="globe icon"></i>
    Referencias
  </h3>
  <div class="content segment">
    <a href="{{ articulo.fuente }}">Inspirado en este artículo</a>
  </div>

  <div class="ui divider"></div>
</div>

<div class="ui main text container" *ngIf="!articulo">
  <div class="ui active inverted dimmer">
    <div
      class="ui massive text loader"
      [ngStyle]="{ 'margin-top': '200px', 'margin-bottom': '60px' }"
    >
      Loading articulo... please be patient
    </div>
  </div>
</div>
```

- Crear componente nueva-receta: $ ng g component components/nueva-receta
- Editar nueva-receta.component.ts

```javascript
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RecetasService } from 'src/app/services/recetas.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-nueva-receta',
  templateUrl: './nueva-receta.component.html',
  styleUrls: ['./nueva-receta.component.css'],
})
export class NuevaRecetaComponent implements OnInit {
  formulario!: FormGroup;
  showPreview = false;
  usuario!: any;

  constructor(
    private fb: FormBuilder,
    private recetasServices: RecetasService,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.formulario = this.fb.group({
      nombre: ['', Validators.required],
      imagen: ['', Validators.required],
      introduccion: ['', Validators.required],
      ingredientes: this.fb.array([this.fb.control('')]),
      preparacion: this.fb.array([this.fb.control('')]),
      consejos: this.fb.array([this.fb.control('')]),
      fuente: ['', Validators.required],
    });
    this.usuario = this.userService.get_user();
  }

  onSubmit() {
    this.recetasServices
      .add_receta({
        autor: this.usuario.uid,
        publicada: new Date(),
        editada: new Date(),
        ...this.formulario.value,
      })
      .then((articulo) => {
        console.log(`submitted: ${JSON.stringify(articulo)}`);
        this.router.navigate([`/articulo/${articulo.id}`]);
      })
      .catch((error) => console.error(error));
  }

  get ingredientes() {
    return this.formulario.get('ingredientes') as FormArray;
  }

  add_ingrediente() {
    this.ingredientes.push(this.fb.control(''));
  }

  delete_ingrediente(i: number) {
    this.ingredientes.removeAt(i);
  }

  get preparacion() {
    return this.formulario.get('preparacion') as FormArray;
  }

  add_preparacion() {
    this.preparacion.push(this.fb.control(''));
  }

  delete_preparacion(i: number) {
    this.preparacion.removeAt(i);
  }

  get consejos() {
    return this.formulario.get('consejos') as FormArray;
  }

  add_consejo() {
    this.consejos.push(this.fb.control(''));
  }

  delete_consejo(i: number) {
    this.consejos.removeAt(i);
  }
}
```

- Editar nueva-receta.component.html

```xml
<div
  class="ui main text container"
  [ngStyle]="{ 'margin-top': '60px', 'margin-bottom': '60px' }"
  *ngIf="!showPreview"
>
  <h2 class="ui header">Publica una nueva receta</h2>
  <!-- Form: {{ formulario.value | json }} -->
  <!-- <div class="ui divider"></div> -->

  <form [formGroup]="formulario" class="ui middle form">
    <div class="ui stacked segment">
      <!-- Nombre de la receta -->
      <div class="field">
        <label class="form-label">Nombre</label>
        <input
          type="text"
          placeholder="Nombre de la receta"
          formControlName="nombre"
          required
        />
      </div>

      <!-- Imagen -->
      <div class="field">
        <label class="form-label">URL de la imagen</label>
        <input
          type="text"
          placeholder="URL de una imagen"
          formControlName="imagen"
          required
        />
      </div>

      <!-- Introducción -->
      <div class="field">
        <label class="form-label">Introducción</label>
        <textarea
          type="text"
          placeholder="Descripción de tu receta"
          formControlName="introduccion"
          required
        ></textarea>
      </div>

      <!-- Ingredientes -->
      <div formArrayName="ingredientes">
        <label class="form-label">Ingredientes</label>
        <div *ngFor="let ingrediente of ingredientes.controls; let i = index">
          <div class="fields">
            <div class="eleven wide field">
              <input
                type="text"
                placeholder="Nuevo ingrediente"
                [formControlName]="i"
                required
              />
            </div>
            <div class="five wide field">
              <button
                class="ui red button form-control"
                (click)="delete_ingrediente(i)"
              >
                Borra este ingrediente
              </button>
            </div>
          </div>
        </div>
      </div>

      <button class="ui medium blue button" (click)="add_ingrediente()">
        Agregar un ingrediente
      </button>

      <!-- Preparación -->
      <div formArrayName="preparacion">
        <label class="form-label">Preparación</label>
        <div *ngFor="let preparacion of preparacion.controls; let i = index">
          <div class="fields">
            <div class="eleven wide field">
              <textarea
                type="text"
                placeholder="Nuevo paso"
                [formControlName]="i"
                required
              ></textarea>
            </div>
            <div class="five wide field">
              <button class="ui red button" (click)="delete_preparacion(i)">
                Borra este paso
              </button>
            </div>
          </div>
        </div>
      </div>

      <button class="ui medium blue button" (click)="add_preparacion()">
        Agrega un paso
      </button>

      <!-- Consejos -->
      <div formArrayName="consejos">
        <label class="form-label">Consejos</label>
        <div *ngFor="let consejo of consejos.controls; let i = index">
          <div class="fields">
            <div class="eleven wide field">
              <input
                type="text"
                placeholder="Nuevo consejo"
                [formControlName]="i"
                required
              />
            </div>
            <div class="five wide field">
              <button class="ui red button" (click)="delete_consejo(i)">
                Borra este consejo
              </button>
            </div>
          </div>
        </div>
      </div>

      <button class="ui medium blue button" (click)="add_consejo()">
        Agrega un consejo
      </button>

      <!-- Fuente -->
      <div class="field">
        <label class="form-label">URL de la fuente</label>
        <input
          type="text"
          placeholder="URL de la fuente, puede ser la de la imagen"
          formControlName="fuente"
          required
        />
      </div>

      <!-- Submit button -->
      <button
        class="ui fluid large primary button"
        (click)="showPreview = !showPreview"
      >
        Pre visualizar
      </button>
    </div>
  </form>
</div>

<!-- Preview, confirm & publish -->
<div
  class="ui main text container"
  [ngStyle]="{ 'margin-top': '60px', 'margin-bottom': '60px' }"
  *ngIf="showPreview"
>
  <h2 class="ui header">{{ formulario.value.nombre | titlecase }}</h2>
  <div class="ui centered medium image">
    <img src="{{ formulario.value.imagen }}" />
  </div>

  <div class="ui divider"></div>

  <h3 class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="newspaper outline icon"></i>
    Introducción
  </h3>
  <div class="ui segment content">
    {{ formulario.value.introduccion }}
  </div>

  <h3 class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="edit outline icon"></i>
    Ingredientes
  </h3>
  <div class="ui segment bulleted list">
    <div *ngFor="let i of formulario.value.ingredientes" class="item">
      {{ i }}
    </div>
  </div>

  <h3 class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="utensils icon"></i>
    Preparación
  </h3>
  <div class="ui segment bulleted list">
    <div *ngFor="let i of formulario.value.preparacion" class="item">
      {{ i }}
    </div>
  </div>

  <h3 *ngIf="formulario.value.consejos" class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="utensil spoon icon"></i>
    Consejos
  </h3>
  <div *ngIf="formulario.value.consejos" class="ui segment bulleted list">
    <div *ngFor="let i of formulario.value.consejos" class="item">
      {{ i }}
    </div>
  </div>

  <h3 class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="globe icon"></i>
    Referencias
  </h3>
  <div class="content segment">
    <a href="{{ formulario.value.fuente }}">Inspirado en este artículo</a>
  </div>

  <div class="ui divider"></div>

  <!-- Botones -->
  <div class="ui buttons">
    <div class="eight wide">
      <button
        class="ui fluid large red button"
        (click)="showPreview = !showPreview"
      >
        Editar
      </button>
    </div>
    <div class="or"></div>
    <div class="eight wide">
      <button class="ui fluid large blue button" (click)="onSubmit()">
        Publicar
      </button>
    </div>
  </div>
</div>
```

- Crear componente editar-receta: $ ng g component components/editar-receta
- Editar editar-receta.component.ts

```javascript
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RecetasService } from 'src/app/services/recetas.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-editar-receta',
  templateUrl: './editar-receta.component.html',
  styleUrls: ['./editar-receta.component.css'],
})
export class EditarRecetaComponent implements OnInit {
  articulo: any = undefined;
  id_articulo!: string;
  formulario!: FormGroup;
  showPreview = false;
  usuario!: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private recetasService: RecetasService,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.id_articulo = this.activatedRoute.snapshot.paramMap.get('id') || '';
    this.recetasService.get_receta(this.id_articulo).then((docSnap) => {
      this.articulo = { id: this.id_articulo, ...docSnap.data() };
      this.setForm();
    });
    this.usuario = this.userService.get_user();
  }

  initForm() {
    this.formulario = this.fb.group({
      nombre: ['', Validators.required],
      imagen: ['', Validators.required],
      introduccion: ['', Validators.required],
      ingredientes: this.fb.array([this.fb.control('')]),
      preparacion: this.fb.array([this.fb.control('')]),
      consejos: this.fb.array([this.fb.control('')]),
      fuente: ['', Validators.required],
    });
  }

  setForm() {
    const values = {
      nombre: this.articulo.nombre,
      imagen: this.articulo.imagen,
      introduccion: this.articulo.introduccion,
      fuente: this.articulo.fuente,
    };
    this.formulario.patchValue(values);
    this.articulo.ingredientes.forEach((value: string, i: number) => {
      this.ingredientes.insert(i, this.fb.control(value));
    });
    this.delete_ingrediente(this.ingredientes.length - 1);
    this.articulo.preparacion.forEach((value: string, i: number) => {
      this.preparacion.insert(i, this.fb.control(value));
    });
    this.delete_preparacion(this.preparacion.length - 1);
    this.articulo.consejos.forEach((value: string, i: number) => {
      this.consejos.insert(i, this.fb.control(value));
    });
    this.delete_consejo(this.consejos.length - 1);
  }

  onSubmit() {
    this.recetasService
      .update_receta({
        autor: this.articulo.autor,
        id: this.id_articulo,
        publicada: this.articulo.publicada,
        editada: new Date(),
        ...this.formulario.value,
      })
      .then(() => {
        this.router.navigate([`/articulo/${this.id_articulo}`]);
      })
      .catch((error) => console.error(error));
  }

  get ingredientes() {
    return this.formulario.get('ingredientes') as FormArray;
  }

  add_ingrediente() {
    this.ingredientes.push(this.fb.control(''));
  }

  delete_ingrediente(i: number) {
    this.ingredientes.removeAt(i);
  }

  get preparacion() {
    return this.formulario.get('preparacion') as FormArray;
  }

  add_preparacion() {
    this.preparacion.push(this.fb.control(''));
  }

  delete_preparacion(i: number) {
    this.preparacion.removeAt(i);
  }

  get consejos() {
    return this.formulario.get('consejos') as FormArray;
  }

  add_consejo() {
    this.consejos.push(this.fb.control(''));
  }

  delete_consejo(i: number) {
    this.consejos.removeAt(i);
  }
}
```

- Editar editar-receta.component.html

```xml
<div
  class="ui main text container"
  [ngStyle]="{ 'margin-top': '60px', 'margin-bottom': '60px' }"
  *ngIf="articulo && usuario && articulo.autor !== usuario.uid"
>
  <h1 class="ui icon header">
    <i class="ban red icon"></i>
    <div class="content">
      Acceso restringido: Solamente el autor puede editar sus receta
    </div>
  </h1>
  <!-- {{ usuario.uid }}
  {{ articulo.autor }} -->
</div>

<div
  class="ui main text container"
  [ngStyle]="{ 'margin-top': '60px', 'margin-bottom': '60px' }"
  *ngIf="!showPreview && articulo.autor === usuario.uid"
>
  <h2 class="ui header">Edita tu receta</h2>
  <!-- <p>articulo is: {{ articulo | json }}</p>
  <div class="ui divider"></div>
  <p>formulario is: {{ formulario.value | json }}</p>
  <div class="ui divider"></div>
  {{ usuario.uid }}
  {{ articulo.autor }}
  <div class="ui divider"></div> -->

  <form [formGroup]="formulario" class="ui middle form">
    <div class="ui stacked segment">
      <!-- Nombre de la receta -->
      <div class="field">
        <label class="form-label">Nombre</label>
        <input
          type="text"
          placeholder="Nombre de la receta"
          formControlName="nombre"
          required
        />
      </div>

      <!-- Imagen -->
      <div class="field">
        <label class="form-label">URL de la imagen</label>
        <input
          type="text"
          placeholder="URL de una imagen"
          formControlName="imagen"
          required
        />
      </div>

      <!-- Introducción -->
      <div class="field">
        <label class="form-label">Introducción</label>
        <textarea
          type="text"
          placeholder="Descripción de tu receta"
          formControlName="introduccion"
          required
        ></textarea>
      </div>

      <!-- Ingredientes -->
      <div formArrayName="ingredientes">
        <label class="form-label">Ingredientes</label>
        <div *ngFor="let ingrediente of ingredientes.controls; let i = index">
          <div class="fields">
            <div class="eleven wide field">
              <input
                type="text"
                placeholder="Nuevo ingrediente"
                [formControlName]="i"
                required
              />
            </div>
            <div class="five wide field">
              <button
                class="ui red button form-control"
                (click)="delete_ingrediente(i)"
              >
                Borra este ingrediente
              </button>
            </div>
          </div>
        </div>
      </div>

      <button class="ui medium blue button" (click)="add_ingrediente()">
        Agregar un ingrediente
      </button>

      <!-- Preparación -->
      <div formArrayName="preparacion">
        <label class="form-label">Preparación</label>
        <div *ngFor="let preparacion of preparacion.controls; let i = index">
          <div class="fields">
            <div class="eleven wide field">
              <textarea
                type="text"
                placeholder="Nuevo paso"
                [formControlName]="i"
                required
              ></textarea>
            </div>
            <div class="five wide field">
              <button class="ui red button" (click)="delete_preparacion(i)">
                Borra este paso
              </button>
            </div>
          </div>
        </div>
      </div>

      <button class="ui medium blue button" (click)="add_preparacion()">
        Agrega un paso
      </button>

      <!-- Consejos -->
      <div formArrayName="consejos">
        <label class="form-label">Consejos</label>
        <div *ngFor="let consejo of consejos.controls; let i = index">
          <div class="fields">
            <div class="eleven wide field">
              <input
                type="text"
                placeholder="Nuevo consejo"
                [formControlName]="i"
                required
              />
            </div>
            <div class="five wide field">
              <button class="ui red button" (click)="delete_consejo(i)">
                Borra este consejo
              </button>
            </div>
          </div>
        </div>
      </div>

      <button class="ui medium blue button" (click)="add_consejo()">
        Agrega un consejo
      </button>

      <!-- Fuente -->
      <div class="field">
        <label class="form-label">URL de la fuente</label>
        <input
          type="text"
          placeholder="URL de la fuente, puede ser la de la imagen"
          formControlName="fuente"
          required
        />
      </div>

      <!-- Submit button -->
      <button
        class="ui fluid large primary button"
        (click)="showPreview = !showPreview"
      >
        Pre visualizar
      </button>
    </div>
  </form>
</div>

<!-- Preview, confirm & publish -->
<div
  class="ui main text container"
  [ngStyle]="{ 'margin-top': '60px', 'margin-bottom': '60px' }"
  *ngIf="showPreview && articulo.autor === usuario.uid"
>
  <h2 class="ui header">{{ formulario.value.nombre | titlecase }}</h2>
  <div class="ui centered medium image">
    <img src="{{ formulario.value.imagen }}" />
  </div>

  <div class="ui divider"></div>

  <h3 class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="newspaper outline icon"></i>
    Introducción
  </h3>
  <div class="ui segment content">
    {{ formulario.value.introduccion }}
  </div>

  <h3 class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="edit outline icon"></i>
    Ingredientes
  </h3>
  <div class="ui segment bulleted list">
    <div *ngFor="let i of formulario.value.ingredientes" class="item">
      {{ i }}
    </div>
  </div>

  <h3 class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="utensils icon"></i>
    Preparación
  </h3>
  <div class="ui segment bulleted list">
    <div *ngFor="let i of formulario.value.preparacion" class="item">
      {{ i }}
    </div>
  </div>

  <h3 *ngIf="formulario.value.consejos" class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="utensil spoon icon"></i>
    Consejos
  </h3>
  <div *ngIf="formulario.value.consejos" class="ui segment bulleted list">
    <div *ngFor="let i of formulario.value.consejos" class="item">
      {{ i }}
    </div>
  </div>

  <h3 class="ui header">
    <i [ngStyle]="{ color: 'gray' }" class="globe icon"></i>
    Referencias
  </h3>
  <div class="content segment">
    <a href="{{ formulario.value.fuente }}">Inspirado en este artículo</a>
  </div>

  <div class="ui divider"></div>

  <!-- Botones -->
  <div class="ui buttons">
    <div class="eight wide">
      <button
        class="ui fluid large red button"
        (click)="showPreview = !showPreview"
      >
        Editar
      </button>
    </div>
    <div class="or"></div>
    <div class="eight wide">
      <button class="ui fluid large blue button" (click)="onSubmit()">
        Publicar
      </button>
    </div>
  </div>
</div>
```

- Agregar nuevas rutas en app.module.ts

```javascript
...
const appRoutes: Routes = [
  { path: '', component: RecetasComponent },
  { path: 'acerca', component: AcercaComponent },
  { path: 'articulo/:id', component: ArticuloComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'entrada', component: EntradaComponent },
  {
    path: 'perfil',
    component: PerfilComponent,
    ...canActivate(() => redirectUnauthorizedTo(['entrada'])),
  },
  {
    path: 'nueva-receta',
    component: NuevaRecetaComponent,
    ...canActivate(() => redirectUnauthorizedTo(['entrada'])),
  },
  {
    path: 'editar-receta/:id',
    component: EditarRecetaComponent,
    ...canActivate(() => redirectUnauthorizedTo(['entrada'])),
  },
];
...
```

## Git

- Actualizar .gitignore:
  - Agregar /src/environments/*
