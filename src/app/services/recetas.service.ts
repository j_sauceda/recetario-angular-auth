import { Injectable } from '@angular/core';
import {
  Firestore,
  addDoc,
  collection,
  collectionData,
  deleteDoc,
  doc,
  getDoc,
  getDocs,
  query,
  setDoc,
  where,
} from '@angular/fire/firestore';
import { Receta } from '../Receta';

@Injectable({
  providedIn: 'root',
})
export class RecetasService {
  constructor(private firestore: Firestore) {}

  get_recetas() {
    const docRef = collection(this.firestore, 'recetas');
    return collectionData(docRef, { idField: 'id' });
  }

  get_receta(id_articulo: string) {
    const docRef = doc(this.firestore, 'recetas', id_articulo);
    return getDoc(docRef);
  }

  get_recetas_autor(id_autor: string) {
    let recetas: any[] = [];
    const q = query(
      collection(this.firestore, 'recetas'),
      where('autor', '==', id_autor)
    );
    const querySnapshot = getDocs(q);
    querySnapshot.then((documents) => {
      documents.forEach((doc) => {
        recetas.push({ id: doc.id, ...doc.data() });
      });
    });
    return recetas;
  }

  add_receta(nueva_receta: Receta) {
    const recetaRef = collection(this.firestore, 'recetas');
    return addDoc(recetaRef, nueva_receta);
  }

  update_receta(receta: Receta) {
    console.log(`Update receta.id: ${receta.id}`);
    const recetaRef = doc(this.firestore, `recetas/${receta.id}`);
    return setDoc(recetaRef, { editada: new Date(), ...receta });
  }

  delete_receta(receta: Receta) {
    const recetaRef = doc(this.firestore, `recetas/${receta.id}`);
    return deleteDoc(recetaRef);
  }
}
