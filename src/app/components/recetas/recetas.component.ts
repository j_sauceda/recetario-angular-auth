import { Component, OnInit } from '@angular/core';
import { RecetasService } from 'src/app/services/recetas.service';

@Component({
  selector: 'app-recetas',
  templateUrl: './recetas.component.html',
  styleUrls: ['./recetas.component.css'],
})
export class RecetasComponent implements OnInit {
  recetas!: any[];

  constructor(private recetasService: RecetasService) {}

  ngOnInit(): void {
    this.recetasService.get_recetas().subscribe((all_recetas) => {
      this.recetas = all_recetas;
    });
    // console.log('recetas contains : ', this.recetas);
  }
}
