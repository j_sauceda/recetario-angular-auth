import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Receta } from 'src/app/Receta';
import { PerfilService } from 'src/app/services/perfil.service';
import { RecetasService } from 'src/app/services/recetas.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css'],
})
export class PerfilComponent implements OnInit {
  formulario!: FormGroup;
  perfil!: any;
  recetas!: any;
  usuario!: any;

  constructor(
    private fb: FormBuilder,
    private perfilService: PerfilService,
    private recetasService: RecetasService,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.usuario = this.userService.get_user();
    this.get_perfil();
    this.get_recetas_perfil();
    this.formulario = this.fb.group({
      nombre: ['', Validators.required],
      imagen: ['', Validators.required],
      acerca: [''],
    });
  }

  get_perfil() {
    this.perfilService.get_perfil(this.usuario.uid).then((docs) => {
      docs.forEach((docSnap) => {
        this.perfil = docSnap.data();
      });
    });
  }

  get_recetas_perfil() {
    this.recetas = this.recetasService.get_recetas_autor(this.usuario.uid);
  }

  onSubmit() {
    this.perfilService
      .add_perfil({
        user: this.usuario.uid,
        ...this.formulario.value,
      })
      .then((new_perfil) => {
        console.log(`Submitted profile: ${JSON.stringify(new_perfil)}`);
        this.get_perfil();
      })
      .catch((error) => console.error(error));
  }

  onUpdate(receta: Receta) {
    this.router.navigate([`/editar-receta/${receta.id}`]);
  }

  onDelete(receta: Receta) {
    this.recetasService.delete_receta(receta).then(() => {
      this.get_recetas_perfil();
      console.log('Receta deleted successfully');
    });
  }
}
