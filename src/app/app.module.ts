import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { canActivate, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { ReactiveFormsModule } from '@angular/forms';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { AcercaComponent } from './components/acerca/acerca.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PieComponent } from './components/pie/pie.component';
import { RecetasComponent } from './components/recetas/recetas.component';
import { ArticuloComponent } from './components/articulo/articulo.component';
import { RegistroComponent } from './components/registro/registro.component';
import { EntradaComponent } from './components/entrada/entrada.component';
import { SalidaComponent } from './components/salida/salida.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { NuevaRecetaComponent } from './components/nueva-receta/nueva-receta.component';
import { EditarRecetaComponent } from './components/editar-receta/editar-receta.component';

const appRoutes: Routes = [
  { path: '', component: RecetasComponent },
  { path: 'acerca', component: AcercaComponent },
  { path: 'articulo/:id', component: ArticuloComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'entrada', component: EntradaComponent },
  {
    path: 'perfil',
    component: PerfilComponent,
    ...canActivate(() => redirectUnauthorizedTo(['entrada'])),
  },
  {
    path: 'nueva-receta',
    component: NuevaRecetaComponent,
    ...canActivate(() => redirectUnauthorizedTo(['entrada'])),
  },
  {
    path: 'editar-receta/:id',
    component: EditarRecetaComponent,
    ...canActivate(() => redirectUnauthorizedTo(['entrada'])),
  },
];

@NgModule({
  declarations: [
    AppComponent,
    AcercaComponent,
    NavbarComponent,
    PieComponent,
    RecetasComponent,
    ArticuloComponent,
    RegistroComponent,
    EntradaComponent,
    SalidaComponent,
    PerfilComponent,
    NuevaRecetaComponent,
    EditarRecetaComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, { enableTracing: false }),
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideAuth(() => getAuth()),
    provideFirestore(() => getFirestore()),
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
